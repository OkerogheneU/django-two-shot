from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Receipt

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})
